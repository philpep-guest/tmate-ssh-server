Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tmate-ssh-server
Upstream-Contact: Nicolas Viennot <nicolas@viennot.com>
Source: https://github.com/tmate-io/tmate-ssh-server

Files: *
Copyright: 2010-2013, Dagobert Michelsen
           2011-2012, George Nachman <tmux@georgester.com>
           2003-2004, Henning Brauer <henning@openbsd.org>
           1995, International Business Machines, Inc
           1996-1998, Internet Software Consortium
           2015, Joerg Jung <jung@openbsd.org>
           2009, Jonathan Alvarado <radobobo@users.sourceforge.net>
           2009, Joshua Elsasser <josh@elsasser.org>
           2011, Marcel P. Partap <mpartap@gmx.net>
           2006-2015, Nicholas Marriott <nicholas.marriott@gmail.com>
           2009, Nicholas Marriott <nicm@openbsd.org>
           2008, Otto Moerbeek <otto@drijf.net>
           2003, Peter Stuge <stuge-mdoc2man@cdy.org>
           2006-2007, Pierre-Yves Ritschard <pyr@openbsd.org>
           2006-2008, Reyk Floeter <reyk@openbsd.org>
           2010, Romain Francoise <rfrancoise@debian.org>
           2004, Ted Unangst and Todd Miller
           2013, Thiago de Arruda <tpadilha84@gmail.com>
           2012, Thomas Adam <thomas@xteddy.org>
           2008-2009, Tiago Cunha <me@tiagocunha.org>
           2014, Tiago Cunha <tcunha@users.sourceforge.net>
           1998-2005, Todd C. Miller <Todd.Miller@courtesan.com>
           2009, Todd Carson <toc@daybefore.net>
License: ISC

Files: .mailmap
       .travis.yml
       CHANGES
       FAQ
       Makefile.am
       README
       README.md
       SYNCING
       TODO
       autogen.sh
       configure.ac
       create_keys.sh
       example_tmux.conf
       monitor/*
       presentations/*
       tmate-daemon-decoder.c
       tmate-daemon-encoder.c
       tmate-daemon-legacy.c
       tmate-debug.c
       tmate-msgpack.c
       tmate-protocol.h
       tmate-proxy.c
       tmate-ssh-client-pty.c
       tmate-ssh-daemon.c
       tmate-ssh-exec.c
       tmate-ssh-latency.c
       tmate-ssh-server.1
       tmate-ssh-server.c
       tmate.h
       tools/*
       window-copy.h
       xmalloc.c
       xmalloc.h
Copyright: 1995, Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
License: ISC

Files: compat/bitstring.h
       compat/daemon.c
       compat/getopt.c
       compat/queue.h
       compat/strcasestr.c
       compat/strsep.c
       compat/unvis.c
       compat/vis.c
       compat/vis.h
Copyright: 1990, The Regents of the University of California
License: BSD-3-clause

Files: compat/fparseln.c
Copyright: 1997, Christos Zoulas
License: BSD-4-clause

Files: compat/tree.h
Copyright: 2002, Niels Provos <provos@citi.umich.edu>
License: BSD-2-clause

Files: logo/*
Copyright: Copyright (c) 2015, Jason Long <jason@jasonlong.me>
           Copyright 2000-2004 Apple Computer Incorporated.
License: ISC

Files: debian/*
Copyright: 2017, Adrian Vondendriesch <adrian.vondendriesch@credativ.de>
License: ISC

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-4-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
 This product includes software developed by Christos Zoulas.
 4. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF MIND, USE, DATA OR PROFITS, WHETHER
 IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
